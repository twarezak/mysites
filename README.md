# MySites

### https://clevertools.online

CleverTools.online contains very useful, clever and free tools useful in everyday life and work.

### https://rgbtohex.app/

rgbtohex.app is used to convert color codes value between two different systems: RGB and Hex (hex triplet). RGB to Hex and Hex to RGB.

### https://sredniawazona.pl/

The Weighted Average Calculator performs calculations based on the entered data. Easy, online and free. Discover the pattern and application.

### https://minikalkulator.pl/

Mini Calculator is a website with various simple calculators that are useful in everyday life. You can quickly and easily calculate interest, VAT, gross and net salary and much more.